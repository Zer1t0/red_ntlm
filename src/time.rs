use crate::Result;
use chrono::{DateTime, TimeZone, Utc, Duration};
use nom::number::complete::le_u64;

pub type NtlmTime = DateTime<Utc>;


const SECONDS_1601_TO_1970: i64 = 0x2b6109100;


/// Creates a NtlmTime for the current moment.
pub fn now() -> NtlmTime {
    return Utc::now();
}


/// Creates a FILETIME timestamp for the current moment.
pub fn now_filetime() -> u64 {
    return datetime_to_filetime(&now());
}

/// Creates a NtlmTime for the given moment.
pub fn new_time(
    year: i32,
    month: u32,
    day: u32,
    hour: u32,
    min: u32,
    sec: u32,
    nano: u32,
) -> NtlmTime {
    return Utc.ymd(year, month, day).and_hms_nano(hour, min, sec, nano);
}


pub fn buid_timestamp(dt: &NtlmTime) -> Vec<u8> {
    return datetime_to_filetime(dt).to_le_bytes().to_vec();
}

pub fn parse_timestamp(raw: &[u8]) -> Result<(&[u8], NtlmTime)> {
    let (raw, ft) = le_u64(raw)?;
    return Ok((raw, filetime_to_datetime(ft)));
}


// The timestamp produced by chrono starts counting seconds in 1970/01/01
// whereas the Windows filetime timestamp is the number of 100 nanosecond
// ticks (hectananoseconds) since midnight of January 1, 1601.
//
// However chrono only handles nanoseconds timestamps from 1677 onward,
// so in order to manage earlier dates (for tests) it is necessary
// to add some dates handling.

/// Converts a Windows Filetime used to transfer the timestamp
/// over the network to a NtlmTime.
pub fn filetime_to_datetime(ft: u64) -> NtlmTime {
    let mut dt = Utc.ymd(1601, 1, 1).and_hms_nano(00, 00, 00, 00);
    let mut ft_ns = (ft as u128) * 100;

    while ft_ns > (i64::MAX as u128) {
        dt = dt + Duration::nanoseconds(i64::MAX);
        ft_ns -= i64::MAX as u128;
    }

    dt = dt + Duration::nanoseconds(ft_ns as i64);
    return dt;
}

/// Converts an NtlmTime to a Windows Filetime used to transfer the timestamp
/// over the network.
pub fn datetime_to_filetime(dt: &NtlmTime) -> u64 {
    let seconds = (dt.timestamp() as i128) + SECONDS_1601_TO_1970 as i128;
    let mut nanoseconds = seconds * 1000000000;

    nanoseconds += dt.timestamp_subsec_nanos() as i128;
    return (nanoseconds / 100) as u64;
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_filetime_to_filetime() {
        for i in 0..10000 {
            assert_eq!(i, datetime_to_filetime(&filetime_to_datetime(i)));
        };
    }
}
