use super::md5;

pub enum SignMode {
    Client,
    Server,
}

impl SignMode {
    pub fn magic_constant(&self) -> &'static str {
        match self {
            Self::Client => {
                "session key to client-to-server signing key magic constant"
            }
            Self::Server => {
                "session key to server-to-client signing key magic constant"
            }
        }
    }

    pub fn magic_constant_bytes(&self) -> Vec<u8> {
        let mut bytes = self.magic_constant().as_bytes().to_vec();
        bytes.push(0);
        return bytes;
    }
}

pub fn signkey(session_key: &[u8], mode: SignMode) -> [u8; 16] {
    let mut t = session_key.to_vec();
    t.extend(mode.magic_constant_bytes());
    return md5(&t);
}

#[cfg(test)]
mod tests {
    use super::*;

    const SIGN_KEY: [u8; 16] = [
        0x47, 0x88, 0xdc, 0x86, 0x1b, 0x47, 0x82, 0xf3, 0x5d, 0x43, 0xfd, 0x98,
        0xfe, 0x1a, 0x2d, 0x39,
    ];

    const RANDOM_SESSION_KEY: [u8; 16] = [
        0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55,
        0x55, 0x55, 0x55, 0x55,
    ];

    #[test]
    fn test_signkey() {
        assert_eq!(SIGN_KEY, signkey(&RANDOM_SESSION_KEY, SignMode::Client));
    }
}
