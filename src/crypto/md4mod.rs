use md4::{Digest, Md4};
use std::convert::TryInto;

pub fn md4(bytes: &[u8]) -> [u8; 16] {
    return Md4::digest(&bytes).try_into().unwrap();
}
