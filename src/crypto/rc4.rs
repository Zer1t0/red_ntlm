use crypto::rc4::Rc4;
use crypto::symmetriccipher::SynchronousStreamCipher;

pub type Rc4Handle = Rc4;

pub fn rc4k(key: &[u8], data: &[u8]) -> Vec<u8> {
    return rc4(&mut rc4init(key), data);
}

pub fn rc4init(key: &[u8]) -> Rc4Handle {
    return Rc4Handle::new(key);
}

pub fn rc4(handle: &mut Rc4Handle, data: &[u8]) -> Vec<u8> {
    let mut result: Vec<u8> = vec![0; data.len()];
    handle.process(data, &mut result);
    return result;
}

#[cfg(test)]
mod tests {
    use super::*;

    const KEY_EXCH_KEY: [u8; 16] = [
        0x8d, 0xe4, 0x0c, 0xca, 0xdb, 0xc1, 0x4a, 0x82, 0xf1, 0x5c, 0xb0, 0xad,
        0x0d, 0xe9, 0x5c, 0xa3,
    ];

    const ENCRYPTED_SESSION_KEY: [u8; 16] = [
        0xc5, 0xda, 0xd2, 0x54, 0x4f, 0xc9, 0x79, 0x90, 0x94, 0xce, 0x1c, 0xe9,
        0x0b, 0xc9, 0xd0, 0x3e,
    ];

    const RANDOM_SESSION_KEY: [u8; 16] = [
        0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55,
        0x55, 0x55, 0x55, 0x55,
    ];

    #[test]
    fn test_rc4k() {
        assert_eq!(
            ENCRYPTED_SESSION_KEY.to_vec(),
            rc4k(&KEY_EXCH_KEY, &RANDOM_SESSION_KEY)
        )
    }
}
