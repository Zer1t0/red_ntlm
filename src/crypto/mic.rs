use crate::crypto::hmac_md5;

pub fn calc_mic(
    session_key: &[u8; 16],
    raw_negotiate: &[u8],
    raw_challenge: &[u8],
    raw_authenticate: &[u8],
) -> [u8; 16] {
    let mut t = raw_negotiate.to_vec();
    t.extend(raw_challenge);
    t.extend(raw_authenticate);

    return hmac_md5(session_key, &t);
}
