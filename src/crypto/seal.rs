use crate::crypto::md5;
use crate::flags::NTLM_NEG_128;
use crate::flags::NTLM_NEG_56;
use crate::flags::NTLM_NEG_DATAGRAM;
use crate::flags::NTLM_NEG_EXTENDED_SECURITY;
use crate::flags::NTLM_NEG_LM_KEY;

pub enum SealMode {
    Client,
    Server,
}

impl SealMode {
    pub fn magic_constant(&self) -> &'static str {
        match self {
            Self::Client => {
                "session key to client-to-server sealing key magic constant"
            }
            Self::Server => {
                "session key to server-to-client sealing key magic constant"
            }
        }
    }

    pub fn magic_constant_bytes(&self) -> Vec<u8> {
        let mut bytes = self.magic_constant().as_bytes().to_vec();
        bytes.push(0);
        return bytes;
    }
}

/// Calculate the key used to seal messages.
pub fn sealkey(
    neg_flags: u32,
    session_key: &[u8; 16],
    mode: SealMode,
) -> Vec<u8> {
    if flag!(neg_flags, NTLM_NEG_EXTENDED_SECURITY) {
        let mut temp = if flag!(neg_flags, NTLM_NEG_128) {
            session_key.to_vec()
        } else if flag!(neg_flags, NTLM_NEG_56) {
            session_key[0..6].to_vec()
        } else {
            session_key[0..4].to_vec()
        };

        temp.extend(mode.magic_constant_bytes());
        return md5(&temp).to_vec();
    } else if flag!(neg_flags, NTLM_NEG_LM_KEY)
        || flag!(neg_flags, NTLM_NEG_DATAGRAM)
    {
        if flag!(neg_flags, NTLM_NEG_56) {
            let mut seal_key = session_key[0..6].to_vec();
            seal_key.push(0xa0);
            return seal_key;
        } else {
            let mut seal_key = session_key[0..4].to_vec();
            seal_key.extend(&[0xe5, 0x38, 0xB0]);
            return seal_key;
        }
    } else {
        return session_key.to_vec();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const SEAL_KEY: &'static [u8] = &[
        0x59, 0xf6, 0x00, 0x97, 0x3c, 0xc4, 0x96, 0x0a, 0x25, 0x48, 0x0a, 0x7c,
        0x19, 0x6e, 0x4c, 0x58,
    ];

    const RANDOM_SESSION_KEY: [u8; 16] = [
        0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55,
        0x55, 0x55, 0x55, 0x55,
    ];

    #[test]
    fn test_sealkey() {
        let flags = NTLM_NEG_EXTENDED_SECURITY | NTLM_NEG_128;
        assert_eq!(SEAL_KEY, sealkey(flags, &RANDOM_SESSION_KEY, SealMode::Client));
    }
}
