use rand::RngCore;
use std::convert::TryInto;

/// Generates an vector with random bytes.
pub fn random_bytes(size: usize) -> Vec<u8> {
    let mut rng = rand::thread_rng();
    let mut bytes: Vec<u8> = vec![0; size];
    rng.fill_bytes(&mut bytes);

    return bytes;
}

pub fn gen_client_challenge() -> [u8; 8] {
    return random_bytes(8).try_into().unwrap();
}

/// Generates a random nonce of the given size.
pub fn nonce(size: usize) -> Vec<u8> {
    return random_bytes(size);
}

