use crypto::md5::Md5;
use crypto::digest::Digest;

/// Calculate the MD5 hash for the given data
pub fn md5(data: &[u8]) -> [u8; 16] {
    let mut md5 = Md5::new();
    let mut output = [0; 16];
    md5.input(data);
    md5.result(&mut output);
    return output;
}
