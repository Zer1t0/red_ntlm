use crate::time;
use crate::time::NtlmTime;
use crate::AvPairs;
use crate::Result;
use nom::bytes::complete::take;
use std::convert::TryInto;

#[derive(Clone, Debug, PartialEq)]
pub enum NtlmResponse {
    V1(Ntlmv1Response),
    V2(Ntlmv2Response),
}

impl NtlmResponse {
    pub fn build(&self) -> Vec<u8> {
        match self {
            Self::V1(n) => n.build(),
            Self::V2(n) => n.build(),
        }
    }
}

impl Default for NtlmResponse {
    fn default() -> Self {
        return Self::V1(Ntlmv1Response::default());
    }
}

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Ntlmv1Response {
    pub response: [u8; 24],
}

impl Ntlmv1Response {
    pub fn build(&self) -> Vec<u8> {
        return self.response.to_vec();
    }

    pub fn parse(raw: &[u8]) -> Result<Self> {
        let (_, raw_response) = take(24usize)(raw)?;

        return Ok(Self {
            response: raw_response.try_into().unwrap(),
        });
    }
}

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Ntlmv2Response {
    pub response: [u8; 16],
    pub client_challenge: NtlmClientChallenge,
}

impl Ntlmv2Response {
    pub fn new(
        response: [u8; 16],
        client_challenge: NtlmClientChallenge,
    ) -> Self {
        return Self {
            response,
            client_challenge,
        };
    }

    pub fn build(&self) -> Vec<u8> {
        let mut raw = self.response.to_vec();
        raw.extend(self.client_challenge.build());

        return raw;
    }

    pub fn parse(raw: &[u8]) -> Result<Self> {
        let (rest, raw_response) = take(16usize)(raw)?;
        let (_, client_challenge) = NtlmClientChallenge::parse(rest)?;

        return Ok(Self {
            response: raw_response.try_into().unwrap(),
            client_challenge,
        });
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct NtlmClientChallenge {
    pub timestamp: NtlmTime,
    pub client_challenge: [u8; 8],
    pub av_pairs: AvPairs,
}

impl Default for NtlmClientChallenge {
    fn default() -> Self {
        return Self::new(time::now(), [0; 8], AvPairs::default());
    }
}

impl NtlmClientChallenge {
    pub fn new(
        timestamp: NtlmTime,
        client_challenge: [u8; 8],
        av_pairs: AvPairs,
    ) -> Self {
        return Self {
            timestamp,
            client_challenge,
            av_pairs,
        };
    }

    pub fn build(&self) -> Vec<u8> {
        let resp_type = 1u8;
        let hi_resp_type = 1u8;
        // Reserved1 = [0, 0]
        // Reserved2 = [0, 0, 0, 0]
        let mut raw = vec![resp_type, hi_resp_type, 0, 0, 0, 0, 0, 0];
        raw.extend(time::buid_timestamp(&self.timestamp));
        raw.extend(&self.client_challenge);

        // Reserved3
        raw.extend(&[0; 4]);
        raw.extend(self.av_pairs.build());

        // reserved
        raw.extend(&[0; 4]);

        return raw;
    }

    pub fn parse(raw: &[u8]) -> Result<(&[u8], Self)> {
        let (raw, _) = take(8usize)(raw)?;
        let (raw, timestamp) = time::parse_timestamp(raw)?;
        let (raw, client_challenge) = take(8usize)(raw)?;
        let (raw, _) = take(4usize)(raw)?;
        let (raw, av_pairs) = AvPairs::parse(raw)?;

        return Ok((
            raw,
            Self::new(
                timestamp,
                client_challenge.try_into().unwrap(),
                av_pairs,
            ),
        ));
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::time::new_time;
    use crate::AvPair;

    const RAW_NTV2_RESPONSE: &'static [u8] = &[
        0x65, 0x63, 0x8a, 0x24, 0xe2, 0x4e, 0xfc, 0xa9, 0xe6, 0x6d, 0xb2, 0xd8,
        0x46, 0xaa, 0xaf, 0x82, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0xa8, 0x58, 0xd9, 0x3c, 0x6a, 0xf1, 0xd6, 0x01, 0x68, 0x44, 0x59, 0x71,
        0x4c, 0x42, 0x6f, 0x77, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x0e, 0x00,
        0x57, 0x00, 0x53, 0x00, 0x30, 0x00, 0x31, 0x00, 0x2d, 0x00, 0x31, 0x00,
        0x30, 0x00, 0x02, 0x00, 0x0e, 0x00, 0x43, 0x00, 0x4f, 0x00, 0x4e, 0x00,
        0x54, 0x00, 0x4f, 0x00, 0x53, 0x00, 0x4f, 0x00, 0x03, 0x00, 0x2a, 0x00,
        0x77, 0x00, 0x73, 0x00, 0x30, 0x00, 0x31, 0x00, 0x2d, 0x00, 0x31, 0x00,
        0x30, 0x00, 0x2e, 0x00, 0x63, 0x00, 0x6f, 0x00, 0x6e, 0x00, 0x74, 0x00,
        0x6f, 0x00, 0x73, 0x00, 0x6f, 0x00, 0x2e, 0x00, 0x6c, 0x00, 0x6f, 0x00,
        0x63, 0x00, 0x61, 0x00, 0x6c, 0x00, 0x04, 0x00, 0x1a, 0x00, 0x63, 0x00,
        0x6f, 0x00, 0x6e, 0x00, 0x74, 0x00, 0x6f, 0x00, 0x73, 0x00, 0x6f, 0x00,
        0x2e, 0x00, 0x6c, 0x00, 0x6f, 0x00, 0x63, 0x00, 0x61, 0x00, 0x6c, 0x00,
        0x05, 0x00, 0x1a, 0x00, 0x63, 0x00, 0x6f, 0x00, 0x6e, 0x00, 0x74, 0x00,
        0x6f, 0x00, 0x73, 0x00, 0x6f, 0x00, 0x2e, 0x00, 0x6c, 0x00, 0x6f, 0x00,
        0x63, 0x00, 0x61, 0x00, 0x6c, 0x00, 0x07, 0x00, 0x08, 0x00, 0xa8, 0x58,
        0xd9, 0x3c, 0x6a, 0xf1, 0xd6, 0x01, 0x09, 0x00, 0x18, 0x00, 0x63, 0x00,
        0x69, 0x00, 0x66, 0x00, 0x73, 0x00, 0x2f, 0x00, 0x57, 0x00, 0x53, 0x00,
        0x30, 0x00, 0x31, 0x00, 0x2d, 0x00, 0x31, 0x00, 0x30, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    ];

    fn gen_ntv2_response() -> Ntlmv2Response {
        Ntlmv2Response {
            response: [
                0x65, 0x63, 0x8a, 0x24, 0xe2, 0x4e, 0xfc, 0xa9, 0xe6, 0x6d,
                0xb2, 0xd8, 0x46, 0xaa, 0xaf, 0x82,
            ],
            client_challenge: NtlmClientChallenge {
                timestamp: new_time(2021, 01, 23, 09, 29, 24, 371268000),
                client_challenge: [
                    0x68, 0x44, 0x59, 0x71, 0x4c, 0x42, 0x6f, 0x77,
                ],
                av_pairs: vec![
                    AvPair::NbComputerName("WS01-10".to_string()),
                    AvPair::NbDomainName("CONTOSO".to_string()),
                    AvPair::DnsComputerName(
                        "ws01-10.contoso.local".to_string(),
                    ),
                    AvPair::DnsDomainName("contoso.local".to_string()),
                    AvPair::DnsTreeName("contoso.local".to_string()),
                    AvPair::Timestamp(new_time(
                        2021, 01, 23, 09, 29, 24, 371268000,
                    )),
                    AvPair::TargetName("cifs/WS01-10".to_string()),
                ]
                .into(),
            },
        }
    }

    #[test]
    fn test_build_ntv2_response() {
        let nt_resp = gen_ntv2_response();
        assert_eq!(RAW_NTV2_RESPONSE.to_vec(), nt_resp.build());
    }

    #[test]
    fn test_parse_ntv2_response() {
        let nt_resp = gen_ntv2_response();
        assert_eq!(nt_resp, Ntlmv2Response::parse(RAW_NTV2_RESPONSE).unwrap());
    }
}
