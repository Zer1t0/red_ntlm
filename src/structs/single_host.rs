use crate::Result;
use nom::bytes::complete::take;
use nom::number::complete::le_u32;
use std::convert::TryInto;

#[derive(Clone, Debug, Default, PartialEq)]
pub struct SingleHost {
    pub data: [u8; 8],
    pub machine_id: [u8; 32],
}

impl SingleHost {
    pub fn build(&self) -> Vec<u8> {
        let mut raw = vec![0x30, 0, 0, 0, 0, 0, 0, 0];
        raw.extend(&self.data);
        raw.extend(&self.machine_id);

        return raw;
    }

    pub fn parse(raw: &[u8]) -> Result<Self> {
        let (raw, _) = le_u32(raw)?;
        let (raw, _) = take(4usize)(raw)?;
        let (raw, data) = take(8usize)(raw)?;
        let (_, machine_id) = take(32usize)(raw)?;

        return Ok(Self {
            data: data.try_into().unwrap(),
            machine_id: machine_id.try_into().unwrap(),
        });
    }
}
