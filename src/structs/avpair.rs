use crate::utils::{from_unicode, to_unicode};
use crate::Result;
use super::SingleHost;
use crate::time;
use crate::time::NtlmTime;
use nom::bytes::complete::take;
use nom::number::complete::{le_u16, le_u32};
use std::convert::TryInto;
use std::slice::Iter;

const AV_EOL: u16 = 0x0000;
const AV_NB_COMPUTER_NAME: u16 = 0x0001;
const AV_NB_DOMAIN_NAME: u16 = 0x0002;
const AV_DNS_COMPUTER_NAME: u16 = 0x0003;
const AV_DNS_DOMAIN_NAME: u16 = 0x0004;
const AV_DNS_TREE_NAME: u16 = 0x0005;
const AV_FLAGS: u16 = 0x0006;
const AV_TIMESTAMP: u16 = 0x0007;
const AV_SINGLE_HOST: u16 = 0x0008;
const AV_TARGET_NAME: u16 = 0x0009;
const AV_CHANNEL_BINDINGS: u16 = 0x000a;

macro_rules! search_av {
    ($self:ident, $type:tt) => {
        for av in $self.iter() {
            match av {
                AvPair::$type(t) => return Some(t),
                _ => {}
            }
        }
        return None;
    };
}

#[derive(Clone, Debug, Default, PartialEq)]
pub struct AvPairs {
    pub pairs: Vec<AvPair>,
}

impl AvPairs {

    /// Returns the NetBIOS computer name if it is in the av_pairs.
    pub fn nb_computer_name(&self) -> Option<&String> {
        search_av!(self, NbComputerName);
    }

    /// Returns the NetBIOS domain name if it is in the av_pairs.
    pub fn nb_domain_name(&self) -> Option<&String> {
        search_av!(self, NbDomainName);
    }

    /// Returns the DNS computer name if it is in the av_pairs.
    pub fn dns_computer_name(&self) -> Option<&String> {
        search_av!(self, DnsComputerName);
    }

    /// Returns the DNS domain name if it is in the av_pairs.
    pub fn dns_domain_name(&self) -> Option<&String> {
        search_av!(self, DnsDomainName);
    }

    /// Returns the DNS tree name (forest name) if it is in the av_pairs.
    pub fn dns_tree_name(&self) -> Option<&String> {
        search_av!(self, DnsTreeName);
    }

    /// Returns the flags if they are in the av_pairs.
    pub fn flags(&self) -> Option<u32> {
        for av in self.iter() {
            match av {
                AvPair::Flags(t) => return Some(*t),
                _ => {}
            }
        }
        return None;
    }

    /// Returns the timestamp if it is in the av_pairs.
    pub fn timestamp(&self) -> Option<&NtlmTime> {
        search_av!(self, Timestamp);
    }

    /// Returns the single host structure if it is in the av_pairs.
    pub fn single_host(&self) -> Option<&SingleHost> {
        search_av!(self, SingleHost);
    }

    /// Returns the target name if it is in the av_pairs.
    pub fn target_name(&self) -> Option<&String> {
        search_av!(self, TargetName);
    }

    /// Returns the channel bindings if they are in the av_pairs.
    pub fn channel_bindings(&self) -> Option<&[u8; 16]> {
        search_av!(self, ChannelBindings);
    }

    pub fn parse(mut raw: &[u8]) -> Result<(&[u8], Self)> {
        let mut pairs = Vec::new();
        loop {
            let (r, ti) = AvPair::parse(raw)?;
            match ti {
                AvPair::EOL => break,
                _ => pairs.push(ti),
            };

            raw = r;
        }

        return Ok((raw, pairs.into()));
    }

    pub fn build(&self) -> Vec<u8> {
        let mut raw = Vec::new();
        for av in self.iter() {
            raw.extend(av.build());
        }

        raw.extend(AvPair::EOL.build());

        return raw;
    }

    pub fn to_vec(self) -> Vec<AvPair> {
        return self.pairs;
    }

    pub fn iter(&self) -> Iter<AvPair> {
        return self.pairs.iter();
    }
}

impl From<Vec<AvPair>> for AvPairs {
    fn from(pairs: Vec<AvPair>) -> Self {
        return Self { pairs };
    }
}

impl Into<Vec<AvPair>> for AvPairs {
    fn into(self) -> Vec<AvPair> {
        return self.to_vec();
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum AvPair {
    NbComputerName(String),
    NbDomainName(String),
    DnsComputerName(String),
    DnsDomainName(String),
    DnsTreeName(String),
    Flags(u32),
    Timestamp(NtlmTime),
    SingleHost(SingleHost),
    TargetName(String),
    ChannelBindings([u8; 16]),
    EOL,
    Raw(u16, Vec<u8>),
}

impl AvPair {
    pub fn parse(raw: &[u8]) -> Result<(&[u8], Self)> {
        let (r, av_id) = le_u16(raw)?;
        let (r, av_len) = le_u16(r)?;
        let (r, av_value) = take(av_len as usize)(r)?;

        let ti = match av_id {
            AV_EOL => AvPair::EOL,
            AV_NB_COMPUTER_NAME => {
                AvPair::NbComputerName(from_unicode(av_value)?)
            }
            AV_NB_DOMAIN_NAME => AvPair::NbDomainName(from_unicode(av_value)?),
            AV_DNS_COMPUTER_NAME => {
                AvPair::DnsComputerName(from_unicode(av_value)?)
            }
            AV_DNS_DOMAIN_NAME => {
                AvPair::DnsDomainName(from_unicode(av_value)?)
            }
            AV_DNS_TREE_NAME => AvPair::DnsTreeName(from_unicode(av_value)?),
            AV_FLAGS => AvPair::Flags(le_u32(av_value)?.1),
            AV_TIMESTAMP => {
                AvPair::Timestamp(time::parse_timestamp(av_value)?.1)
            }
            AV_SINGLE_HOST => AvPair::SingleHost(SingleHost::parse(av_value)?),
            AV_TARGET_NAME => AvPair::TargetName(from_unicode(av_value)?),
            AV_CHANNEL_BINDINGS => AvPair::ChannelBindings(
                take(16usize)(av_value)?.1.try_into().unwrap(),
            ),
            _ => AvPair::Raw(av_id, av_value.to_vec()),
        };

        return Ok((r, ti));
    }

    pub fn build(&self) -> Vec<u8> {
        let (av_id, av_value) = match self {
            Self::EOL => (AV_EOL, Vec::new()),
            Self::NbComputerName(n) => (AV_NB_COMPUTER_NAME, to_unicode(&n)),
            Self::NbDomainName(n) => (AV_NB_DOMAIN_NAME, to_unicode(&n)),
            Self::DnsComputerName(n) => (AV_DNS_COMPUTER_NAME, to_unicode(&n)),
            Self::DnsDomainName(n) => (AV_DNS_DOMAIN_NAME, to_unicode(&n)),
            Self::DnsTreeName(n) => (AV_DNS_TREE_NAME, to_unicode(&n)),
            Self::Flags(f) => (AV_FLAGS, f.to_le_bytes().to_vec()),
            Self::Timestamp(t) => (AV_TIMESTAMP, time::buid_timestamp(t)),
            Self::SingleHost(s) => (AV_SINGLE_HOST, s.build()),
            Self::TargetName(t) => (AV_TARGET_NAME, to_unicode(&t)),
            Self::ChannelBindings(c) => (AV_CHANNEL_BINDINGS, c.to_vec()),
            Self::Raw(c, v) => (*c, v.to_vec()),
        };

        let mut raw = av_id.to_le_bytes().to_vec();
        raw.extend(&(av_value.len() as u16).to_le_bytes());
        raw.extend(av_value);

        return raw;
    }
}
