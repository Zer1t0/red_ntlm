use crate::Result;
use nom::bytes::complete::take;
use std::convert::TryInto;

#[derive(Clone, Debug, PartialEq)]
pub enum LmResponse {
    V1(Lmv1Response),
    V2(Lmv2Response),
}

impl LmResponse {
    pub fn build(&self) -> Vec<u8> {
        match self {
            Self::V1(lm) => lm.build(),
            Self::V2(lm) => lm.build(),
        }
    }
}

impl Default for LmResponse {
    fn default() -> Self {
        return Self::V1(Lmv1Response::default());
    }
}

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Lmv1Response {
    pub response: [u8; 24],
}

impl Lmv1Response {
    pub fn new(response: [u8; 24]) -> Self {
        return Self { response };
    }

    pub fn build(&self) -> Vec<u8> {
        return self.response.to_vec();
    }

    pub fn parse(raw: &[u8]) -> Result<Self> {
        let (_, raw_response) = take(24usize)(raw)?;
        return Ok(Self {
            response: raw_response.try_into().unwrap(),
        });
    }
}

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Lmv2Response {
    pub response: [u8; 16],
    pub client_challenge: [u8; 8],
}

impl Lmv2Response {
    pub fn new(response: [u8; 16], client_challenge: [u8; 8]) -> Self {
        return Self {
            response,
            client_challenge,
        };
    }

    pub fn build(&self) -> Vec<u8> {
        let mut raw = self.response.to_vec();
        raw.extend(&self.client_challenge);
        return raw;
    }

    pub fn parse(raw: &[u8]) -> Result<Self> {
        let (rest, raw_response) = take(16usize)(raw)?;
        let (_, raw_cch) = take(8usize)(rest)?;

        return Ok(Self {
            response: raw_response.try_into().unwrap(),
            client_challenge: raw_cch.try_into().unwrap()
        });
    }
}
