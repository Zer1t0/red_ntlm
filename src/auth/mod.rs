
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum NtlmAuth {
    V1,
    V2,
}

pub mod v2;

