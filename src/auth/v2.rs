use crate::crypto::hmac_md5;
use crate::time::NtlmTime;
use crate::utils::to_unicode;
use crate::AvPairs;
use crate::Lmv2Response;
use crate::NtlmClientChallenge;
use crate::Ntlmv2Response;

pub fn ntowfv2(nt_hash: &[u8], user: &str, domain: &str) -> [u8; 16] {
    return hmac_md5(
        nt_hash,
        &to_unicode(&format!("{}{}", user.to_uppercase(), domain)),
    );
}

pub fn lmowfv2(nt_hash: &[u8], user: &str, domain: &str) -> [u8; 16] {
    return ntowfv2(nt_hash, user, domain);
}

pub fn calc_response_key_nt(
    nt_hash: &[u8],
    user: &str,
    domain: &str,
) -> [u8; 16] {
    return ntowfv2(nt_hash, user, domain);
}

pub fn calc_response_key_lm(
    nt_hash: &[u8],
    user: &str,
    domain: &str,
) -> [u8; 16] {
    return lmowfv2(nt_hash, user, domain);
}

pub fn calc_temp(
    timestamp: NtlmTime,
    client_challenge: [u8; 8],
    av_pairs: AvPairs,
) -> NtlmClientChallenge {
    return NtlmClientChallenge::new(timestamp, client_challenge, av_pairs);
}

pub fn calc_nt_proof_str(
    response_key_nt: &[u8],
    server_challenge: &[u8],
    temp: &[u8],
) -> [u8; 16] {
    let mut raw = server_challenge.to_vec();
    raw.extend(temp);
    return hmac_md5(response_key_nt, &raw);
}

pub fn calc_session_base_key(
    response_key_nt: &[u8],
    nt_proof_str: &[u8],
) -> [u8; 16] {
    return hmac_md5(response_key_nt, nt_proof_str);
}

pub fn calc_nt_response(
    nt_proof_str: [u8; 16],
    temp: NtlmClientChallenge,
) -> Ntlmv2Response {
    return Ntlmv2Response::new(nt_proof_str, temp);
}

pub fn calc_lm_response(
    response_key_lm: &[u8],
    server_challenge: &[u8],
    client_challenge: [u8; 8],
) -> Lmv2Response {
    let mut cc_scc = server_challenge.to_vec();
    cc_scc.extend(&client_challenge);

    return Lmv2Response::new(
        hmac_md5(response_key_lm, &cc_scc),
        client_challenge,
    );
}

// retrieves ntchallenge, lmchallenge and session basekey
/// Function to calculate NTLM Response, LM Response and base session key
/// used in NTLM authentication V2.
pub fn compute_response_v2(
    _flags: u32,
    response_key_nt: &[u8],
    response_key_lm: &[u8],
    server_challenge: &[u8],
    client_challenge: [u8; 8],
    timestamp: NtlmTime,
    av_pairs: AvPairs,
) -> (Ntlmv2Response, Lmv2Response, [u8; 16]) {
    let temp = calc_temp(timestamp, client_challenge, av_pairs);
    let raw_temp = temp.build();

    let nt_proof_str =
        calc_nt_proof_str(response_key_nt, server_challenge, &raw_temp);
    let nt_challenge_response = calc_nt_response(nt_proof_str, temp);

    let lm_challenge_response =
        calc_lm_response(response_key_lm, server_challenge, client_challenge);

    let session_base_key =
        calc_session_base_key(response_key_nt, &nt_proof_str);

    return (
        nt_challenge_response,
        lm_challenge_response,
        session_base_key,
    );
}

/// Calculate the key exchange key for NTLM v2
pub fn kxkey_v2(
    session_base_key: &[u8; 16],
    _lm_challenge_response: &[u8],
    _server_challenge: &[u8],
) -> [u8; 16] {
    return session_base_key.clone();
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::crypto::hash_nt;
    use crate::time;

    const RAW_AV_PAIRS: &'static [u8] = &[
        0x02, 0x00, 0x0c, 0x00, 0x44, 0x00, 0x6f, 0x00, 0x6d, 0x00, 0x61, 0x00,
        0x69, 0x00, 0x6e, 0x00, 0x01, 0x00, 0x0c, 0x00, 0x53, 0x00, 0x65, 0x00,
        0x72, 0x00, 0x76, 0x00, 0x65, 0x00, 0x72, 0x00, 0x00, 0x00, 0x00, 0x00,
    ];

    const RESPONSE_KEY_NT: [u8; 16] = [
        0x0c, 0x86, 0x8a, 0x40, 0x3b, 0xfd, 0x7a, 0x93, 0xa3, 0x00, 0x1e, 0xf2,
        0x2e, 0xf0, 0x2e, 0x3f,
    ];

    const RESPONSE_KEY_LM: [u8; 16] = RESPONSE_KEY_NT;

    const RAW_TEMP: &'static [u8] = &[
        0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa,
        0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x0c, 0x00, 0x44, 0x00, 0x6f, 0x00,
        0x6d, 0x00, 0x61, 0x00, 0x69, 0x00, 0x6e, 0x00, 0x01, 0x00, 0x0c, 0x00,
        0x53, 0x00, 0x65, 0x00, 0x72, 0x00, 0x76, 0x00, 0x65, 0x00, 0x72, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    ];

    const CLIENT_CHALLENGE: [u8; 8] = [0xaa; 8];

    const SERVER_CHALLENGE: [u8; 8] =
        [0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef];

    const RAW_TIME: u64 = 0x000000000000000000;

    const SESSION_KEY: [u8; 16] = [
        0x8d, 0xe4, 0x0c, 0xca, 0xdb, 0xc1, 0x4a, 0x82, 0xf1, 0x5c, 0xb0, 0xad,
        0x0d, 0xe9, 0x5c, 0xa3,
    ];

    const LMV2_RESPONSE: &'static [u8] = &[
        0x86, 0xc3, 0x50, 0x97, 0xac, 0x9c, 0xec, 0x10, 0x25, 0x54, 0x76, 0x4a,
        0x57, 0xcc, 0xcc, 0x19, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa,
    ];

    const NT_PROOF_STR: &'static [u8] = &[
        0x68, 0xcd, 0x0a, 0xb8, 0x51, 0xe5, 0x1c, 0x96, 0xaa, 0xbc, 0x92, 0x7b,
        0xeb, 0xef, 0x6a, 0x1c,
    ];

    #[test]
    fn test_calc_response_key_nt() {
        assert_eq!(
            RESPONSE_KEY_NT,
            calc_response_key_nt(&hash_nt("Password"), "User", "Domain")
        )
    }

    #[test]
    fn test_calc_response_key_lm() {
        assert_eq!(
            RESPONSE_KEY_LM,
            calc_response_key_lm(&hash_nt("Password"), "User", "Domain")
        )
    }

    #[test]
    fn test_calc_temp() {
        assert_eq!(
            RAW_TEMP,
            calc_temp(
                time::filetime_to_datetime(RAW_TIME),
                CLIENT_CHALLENGE,
                AvPairs::parse(RAW_AV_PAIRS).unwrap().1
            )
            .build()
        )
    }

    #[test]
    fn test_calc_session_key() {
        let nt_proof =
            calc_nt_proof_str(&RESPONSE_KEY_NT, &SERVER_CHALLENGE, &RAW_TEMP);
        assert_eq!(
            SESSION_KEY,
            calc_session_base_key(&RESPONSE_KEY_NT, &nt_proof)
        )
    }

    #[test]
    fn test_calc_lm_response() {
        assert_eq!(
            LMV2_RESPONSE,
            calc_lm_response(
                &RESPONSE_KEY_LM,
                &SERVER_CHALLENGE,
                CLIENT_CHALLENGE
            )
            .build()
        )
    }

    #[test]
    fn test_calc_nt_response() {
        assert_eq!(
            NT_PROOF_STR,
            calc_nt_proof_str(&RESPONSE_KEY_NT, &SERVER_CHALLENGE, &RAW_TEMP),
        )
    }
}
