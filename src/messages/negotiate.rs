use crate::messages::NTLM_SIGN;
use nom::number::complete::{le_u32, le_u16};
use nom::bytes::complete::{tag, take};
use crate::Version;
use crate::utils;
use crate::flags::NTLM_NEG_VERSION;
use crate::Result;

const NTLM_MSG_NEGOTIATE: u32 = 0x00000001;

/// Negotiate NTLM message. It is the first message that is sent in a NTLM
/// authentication.
///
/// It is defined in [MS-NLMP] section 2.2.1.
#[derive(Clone, Debug, Default, PartialEq)]
pub struct NegotiateMsg {
    pub flags: u32,

    /// The domain of the host that sends the message.
    pub domain: Option<String>,

    /// The name of the host that sends the message.
    pub workstation: Option<String>,

    /// The Operating System version of the host that sends the message. It
    /// must be sent when NTLM_NEG_VERSION is set.
    pub version: Option<Version>,
}

impl NegotiateMsg {

    /// Indicates if the given flag is set in the message.
    pub fn has_flag(&self, flag: u32) -> bool {
        return (&self.flags & flag) != 0;
    }

    /// Create the raw representation in bytes to be transmitted over the
    /// network.
    pub fn build(&self) -> Vec<u8> {
        let mut raw = NTLM_SIGN.to_vec();
        raw.extend(&NTLM_MSG_NEGOTIATE.to_le_bytes());
        raw.extend(&self.flags.to_le_bytes());

        let mut offset: u32 = 32;

        let raw_version = match &self.version {
            Some(v) => v.build(),
            None => Vec::new()
        };
        offset += raw_version.len() as u32;

        let (raw_dn, dn_offset) = match &self.domain {
            Some(dn) => (utils::to_oem(dn), offset),
            None => (Vec::new(), 0)
        };
        offset += raw_dn.len() as u32;

        let (raw_ws, ws_offset) = match &self.workstation {
            Some(ws) => (utils::to_oem(ws), offset),
            None => (Vec::new(), 0)
        };

        enc_len_offset!(raw, raw_dn, dn_offset);
        enc_len_offset!(raw, raw_ws, ws_offset);

        raw.extend(raw_version);
        raw.extend(raw_dn);
        raw.extend(raw_ws);

        return raw;
    }

    /// Parse network bytes to build a Negotiate message.
    pub fn parse(raw: &[u8]) -> Result<Self> {
        let (rest, _) = tag(NTLM_SIGN)(raw)?;
        let (rest, _) = tag(NTLM_MSG_NEGOTIATE.to_le_bytes())(rest)?;

        let (rest, flags) = le_u32(rest)?;

        let (rest, dn_len) = le_u16(rest)?;
        let (rest, _) = le_u16(rest)?;
        let (rest, dn_offset) = le_u32(rest)?;

        let (rest, ws_len) = le_u16(rest)?;
        let (rest, _) = le_u16(rest)?;
        let (rest, ws_offset) = le_u32(rest)?;

        let version = if flag!(flags, NTLM_NEG_VERSION) {
            Some(Version::parse(rest)?.1)
        } else {
            None
        };

        let domain = match dn_len {
            0 => None,
            _ => {
                let (rest_dn, _) = take(dn_offset as usize)(raw)?;
                let (_, raw_dn) = take(dn_len as usize)(rest_dn)?;
                Some(utils::from_oem(raw_dn.to_vec())?)
            }
        };

        let workstation = match ws_len {
            0 => None,
            _ => {
                let (rest_ws, _) = take(ws_offset as usize)(raw)?;
                let (_, raw_ws) = take(ws_len as usize)(rest_ws)?;
                Some(utils::from_oem(raw_ws.to_vec())?)
            }
        };

        return Ok(Self {
            flags,
            version,
            workstation,
            domain,
        });
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::flags;

    const RAW_NEG: &'static [u8] = &[
        0x4e, 0x54, 0x4c, 0x4d, 0x53, 0x53, 0x50, 0x00, 0x01, 0x00, 0x00, 0x00,
        0x97, 0x82, 0x08, 0xe2, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x01, 0xb1, 0x1d,
        0x00, 0x00, 0x00, 0x0f,
    ];

    #[test]
    fn test_build_ntlm_neg() {
        let mut neg = NegotiateMsg::default();
        neg.flags = flags::WINDOWS_7_NEG_FLAGSET;
        neg.version = Some(Version::windows7_7601());
        assert_eq!(RAW_NEG.to_vec(), neg.build());
    }

    #[test]
    fn test_parse_ntlm_neg() {
        let mut neg = NegotiateMsg::default();
        neg.flags = flags::WINDOWS_7_NEG_FLAGSET;
        neg.version = Some(Version::windows7_7601());
        assert_eq!(neg, NegotiateMsg::parse(RAW_NEG).unwrap());
    }
}
