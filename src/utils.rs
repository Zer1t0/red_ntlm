use super::flags::NTLM_NEG_UNICODE;
use super::Result;
use nom::number::complete::le_u16;

pub fn from_unicode(raw: &[u8]) -> Result<String> {
    let mut uni_chars = Vec::new();
    let mut rest = raw;
    while !rest.is_empty() {
        let (r, unichar) = le_u16(rest)?;
        uni_chars.push(unichar);
        rest = r;
    }

    return Ok(String::from_utf16(&uni_chars)?);
}

pub fn to_unicode(s: &str) -> Vec<u8> {
    let mut s_utf16 = Vec::new();

    for c in s.encode_utf16() {
        s_utf16.extend(&c.to_le_bytes());
    }
    return s_utf16;
}

pub fn from_oem(raw: Vec<u8>) -> Result<String> {
    return Ok(String::from_utf8(raw)?);
}

pub fn to_oem(s: &str) -> Vec<u8> {
    return s.as_bytes().to_vec();
}

/// Transforms a string into Unicode or OEM encoded bytes depending on the
/// negotiated flags.
pub fn to_neg_enc(s: &str, flags: u32) -> Vec<u8> {
    if (flags & NTLM_NEG_UNICODE) != 0 {
        return to_unicode(s);
    }

    return to_oem(s);
}

/// Transforms Unicode or OEM encoded bytes, depending on the
/// negotiated flags, into a string.
pub fn from_neg_enc(raw: &[u8], flags: u32) -> Result<String> {
    if (flags & NTLM_NEG_UNICODE) != 0 {
        return from_unicode(raw);
    }

    return from_oem(raw.to_vec());
}

/// Macro to check if a flag is set in flags field of NTLM messages
macro_rules! flag {
    ($flags:expr, $flag:expr) => {
        ($flags & $flag) != 0
    };
}

/// Macro to encode those [ Length | Max Length | Offset ] blocks
macro_rules! enc_len_offset {
    ($raw:expr, $data:expr, $offset:expr) => {
        $raw.extend(&($data.len() as u16).to_le_bytes());
        $raw.extend(&($data.len() as u16).to_le_bytes());
        $raw.extend(&$offset.to_le_bytes());
    };
}

/// Macro to decode those [ Length | Max Length | Offset ] blocks
macro_rules! dec_len_offset {
    ($raw:expr) => {{
        let (rest, raw_len) = le_u16($raw)?;
        let (rest, _) = le_u16(rest)?;
        let (rest, raw_offset) = le_u32(rest)?;
        (rest, raw_len, raw_offset)
    }};
}
