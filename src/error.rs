use thiserror::Error;
use nom;
use std::string::{FromUtf16Error, FromUtf8Error};

pub type Result<T> = std::result::Result<T, Error>;


#[derive(Debug, Error, PartialEq)]
pub enum Error {
    #[error("No enough data was received")]
    NotEnoughData,

    #[error("{0}")]
    GenericParserError(String),

    #[error("{0}")]
    InvalidUtf16(String),

    #[error("{0}")]
    InvalidUtf8(String),
}

impl From<FromUtf8Error> for Error {
    fn from (e: FromUtf8Error) -> Self {
        return Self::InvalidUtf8(e.to_string());
    }
}

impl From<FromUtf16Error> for Error {
    fn from (e: FromUtf16Error) -> Self {
        return Self::InvalidUtf16(e.to_string());
    }
}

impl From<nom::Err<nom::error::Error<& [u8]>>> for Error {
    fn from(e: nom::Err<nom::error::Error<& [u8]>>) -> Self {
        if let nom::Err::Error(ref e) = e {
            if e.code == nom::error::ErrorKind::Eof {
                return Self::NotEnoughData;
            }
        }
        return Self::GenericParserError(e.to_string());
    }
}
